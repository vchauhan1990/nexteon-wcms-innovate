package com.nexteon.innovate.model;

import java.util.List;

import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.persistence.entity.Delegate_Registration;
import com.nexteon.innovate.service.api.Delegate_RegistrationService;
import com.nexteonsolutions.aembootstrap.core.models.support.ModelSupport;

@Model(adaptables = { SlingHttpServletRequest.class })
public class InnovateSaveModel extends ModelSupport {

	private static final Logger LOGGER = LoggerFactory.getLogger(InnovateSaveModel.class);

	@OSGiService
	private Delegate_RegistrationService registrationService;

	List<Delegate_Registration> registrationPerformaList;
	
	public List<Delegate_Registration> getRegistrationPerformaList() {
		registrationPerformaList = this.registrationService.getAllRegistrations();
		return registrationPerformaList;
	}
}