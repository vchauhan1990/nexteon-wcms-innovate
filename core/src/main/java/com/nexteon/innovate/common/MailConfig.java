package com.nexteon.innovate.common;

import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class MailConfig {

	public MimeMessage mimeMessage(){
		
        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.host", "smtp.gmail.com");
        javaMailProperties.put("mail.smtp.socketFactory.port", "465");
        javaMailProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        javaMailProperties.put("mail.smtp.auth", "true");
        javaMailProperties.put("mail.smtp.port", "465");       
        
        Session session = Session.getInstance(javaMailProperties,    
                new javax.mail.Authenticator() {    
                protected PasswordAuthentication getPasswordAuthentication() {    
                return new PasswordAuthentication("innovatetoinspire@eesl.co.in","inspire@123");  
                }    
               });
        MimeMessage message = new MimeMessage(session); 
        return message;
	}
	
}
