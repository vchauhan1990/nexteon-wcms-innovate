package com.nexteon.innovate.activator;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InnovateActivator implements BundleActivator{

	private static final Logger LOGGER = LoggerFactory.getLogger(InnovateActivator.class);
	
	@Override
	public void start(BundleContext context) throws Exception {
		LOGGER.debug("Starting ard core bundle");	
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		LOGGER.debug("Stoping ard core bundle");
	}

}
