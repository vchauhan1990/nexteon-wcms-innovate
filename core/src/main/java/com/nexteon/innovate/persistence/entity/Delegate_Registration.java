package com.nexteon.innovate.persistence.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.nexteon.innovate.persistence.api.BaseEntity;

@Entity
@Table(name="Delegate_Registration", schema = "dbo", uniqueConstraints=@UniqueConstraint(columnNames={"Email","MobileNumber"}))
public class Delegate_Registration implements BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2663829359666398995L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int ID;
	
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date CreateDate;
	
	private String DelegateName;
	
	private String Designation;
	
	private String Organisation;

	private String Address;
	
	private String City;
	
	private String PinCode;
	
	private String Phone_Number;
	
	private String Fax;
	
	private String MobileNumber;
	
	private String Email;
	
	private boolean IsActive;

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Date getCreateDate() {
		return CreateDate;
	}

	public void setCreateDate(Date createDate) {
		CreateDate = createDate;
	}

	public String getDelegateName() {
		return DelegateName;
	}

	public void setDelegateName(String delegateName) {
		DelegateName = delegateName;
	}

	public String getDesignation() {
		return Designation;
	}

	public void setDesignation(String designation) {
		Designation = designation;
	}

	public String getOrganisation() {
		return Organisation;
	}

	public void setOrganisation(String organisation) {
		Organisation = organisation;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getPinCode() {
		return PinCode;
	}

	public void setPinCode(String pinCode) {
		PinCode = pinCode;
	}

	public String getPhone_Number() {
		return Phone_Number;
	}

	public void setPhone_Number(String phone_Number) {
		Phone_Number = phone_Number;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public boolean isIsActive() {
		return IsActive;
	}

	public void setIsActive(boolean isActive) {
		IsActive = isActive;
	}

}
