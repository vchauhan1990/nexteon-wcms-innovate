package com.nexteon.innovate.persistence.provider.impl;

import java.io.Closeable;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Wrapper to auto close the EntityManager and EntityTransaction.
 * 
 * @author Rakesh.Kumar NextEon
 *
 */
public class EntityManagerDelegate implements Closeable {

	private EntityManager delegatee;

	private EntityTransaction transaction;

	public EntityManagerDelegate(EntityManager delegatee, boolean withTxn) {
		this.delegatee = delegatee;
		if (withTxn) {
			this.transaction = this.delegatee.getTransaction();
			this.transaction.begin();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() {
		if (this.transaction != null && this.transaction.isActive()) {
			this.transaction.rollback();
		}
		if (this.delegatee != null) {
			this.delegatee.close();
		}
	}

	public EntityManager getDelegatee() {
		return delegatee;
	}

	public EntityTransaction getTransaction() {
		return transaction;
	}

}
