package com.nexteon.innovate.persistence.provider.impl;

/**
 * Holder for JPA properties.
 * 
 * @author Rakesh.Kumar, NextEon Technologies.
 * @since 15.11.2014
 */
public class JpaProperties {

	/** The persistence unit. */
	private String persistenceUnit;

	private String jdbcDriver;

	/** The jdbc url. */
	private String jdbcUrl;

	/** The jdbc user. */
	private String jdbcUser;

	/** The jdbc password. */
	private String jdbcPassword;

	/** The EclipseLink logging level. */
	private String loggingLevel;

	private String persistenseXml;

	/**
	 * Initialize all the fields.
	 * 
	 * @param persistenceUnit
	 * @param jdbcDriver
	 * @param jdbcUrl
	 * @param jdbcUser
	 * @param jdbcPassword
	 * @param loggingLevel
	 */
	public JpaProperties(String persistenceUnit, String jdbcDriver, String jdbcUrl, String jdbcUser,
			String jdbcPassword, String loggingLevel, String persistenseXml) {
		this.persistenceUnit = persistenceUnit;
		this.jdbcDriver = jdbcDriver;
		this.jdbcUrl = jdbcUrl;
		this.jdbcUser = jdbcUser;
		this.jdbcPassword = jdbcPassword;
		this.loggingLevel = loggingLevel;
		this.persistenseXml = persistenseXml;
	}

	/**
	 * @return the persistenceUnit
	 */
	public String getPersistenceUnit() {
		return persistenceUnit;
	}

	/**
	 * @param persistenceUnit
	 *            the persistenceUnit to set
	 */
	public void setPersistenceUnit(String persistenceUnit) {
		this.persistenceUnit = persistenceUnit;
	}

	public String getJdbcDriver() {
		return jdbcDriver;
	}

	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	/**
	 * @return the jdbcUrl
	 */
	public String getJdbcUrl() {
		return jdbcUrl;
	}

	/**
	 * @param jdbcUrl
	 *            the jdbcUrl to set
	 */
	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	/**
	 * @return the jdbcUser
	 */
	public String getJdbcUser() {
		return jdbcUser;
	}

	/**
	 * @param jdbcUser
	 *            the jdbcUser to set
	 */
	public void setJdbcUser(String jdbcUser) {
		this.jdbcUser = jdbcUser;
	}

	/**
	 * @return the jdbcPassword
	 */
	public String getJdbcPassword() {
		return jdbcPassword;
	}

	/**
	 * @param jdbcPassword
	 *            the jdbcPassword to set
	 */
	public void setJdbcPassword(String jdbcPassword) {
		this.jdbcPassword = jdbcPassword;
	}

	/**
	 * @return the loggingLevel
	 */
	public String getLoggingLevel() {
		return loggingLevel;
	}

	/**
	 * @param loggingLevel
	 *            the loggingLevel to set
	 */
	public void setLoggingLevel(String loggingLevel) {
		this.loggingLevel = loggingLevel;
	}

	public String getPersistenseXml() {
		return persistenseXml;
	}

	public void setPersistenseXml(String persistenseXml) {
		this.persistenseXml = persistenseXml;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "JpaProperties [persistenceUnit=" + persistenceUnit + ", jdbcDriver=" + jdbcDriver + ", jdbcUrl="
				+ jdbcUrl + ", jdbcUser=" + jdbcUser + ", loggingLevel=" + loggingLevel + "]";
	}

}