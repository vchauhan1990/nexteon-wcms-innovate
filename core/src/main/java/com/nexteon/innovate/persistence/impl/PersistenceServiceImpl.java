/**
 * 
 */
package com.nexteon.innovate.persistence.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.eclipse.persistence.exceptions.EclipseLinkException;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.persistence.api.BaseEntity;
import com.nexteon.innovate.persistence.api.PersistenceService;
import com.nexteon.innovate.persistence.provider.EntityManagerProviderFactory;
import com.nexteon.innovate.persistence.provider.PersistanceProviderService;
import com.nexteon.innovate.persistence.provider.impl.EntityManagerDelegate;




/**
 * This class provides the CRUD implementation. JPA 2.1 (EclipseLink 2.5.2) is
 * used by this class to carry out CRUD operations.
 *
 * @author Rakesh.Kumar, NextEon Technologies.
 * @since 15.11.2014
 */
@Service(PersistenceService.class)
@Component(metatype = true, immediate = true, label = "PersistenceService for InnovateToInspire")
@Properties({ @Property(name = Constants.SERVICE_VENDOR, value = "NextEon IT Soltuions"),
		@Property(name = Constants.SERVICE_DESCRIPTION, value = "InnovateToInspire PersistenceService for CRUD operations.") })
public class PersistenceServiceImpl implements PersistenceService {

	/**
	 * Whether NamedQuery is supported.
	 */
	@Property(label = "Whether NamedQuery is supported", boolValue = false)
	public static final String NAMED_QUERY_SUPPORTED = "named.query.supported";

	/**
	 * Flag to denote whether the NamedQuery is supported.
	 */
	private boolean isNamedQuerySupported;

	/**
	 * Logger for this class.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceServiceImpl.class);

	/**
	 * EntityManagerProviderFactory OSGI reference injected.
	 */
	@Reference
	private EntityManagerProviderFactory emProvider;

	@Reference
	private PersistanceProviderService providerService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> T insert(T transientObj, String persistenseUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, true)) {
			delegate.getDelegatee().persist(transientObj);
			delegate.getTransaction().commit();
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while inserting :", ex);
		}
		return transientObj;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> T update(T persistentObj, String persistenseUnit) {
		LOGGER.info("Entity to be updated :" + persistentObj);
		T updatedObj = null;
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, true)) {
			updatedObj = delegate.getDelegatee().merge(persistentObj);
			delegate.getTransaction().commit();
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
		}
		return updatedObj;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> int updateByCriteria(Class<T> entity, Map<String, Object> predicateMap,
			Map<String, Object> updateFields, String persistenseUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, true)) {
			EntityManager em = delegate.getDelegatee();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaUpdate<T> update = cb.createCriteriaUpdate(entity);
			for (Entry<String, ?> entry : updateFields.entrySet()) {
				update.set(entry.getKey(), entry.getValue());
			}
			List<Predicate> predicates = this.getPredicates(predicateMap, cb, update.from(entity));
			update.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			int rowCount = em.createQuery(update).executeUpdate();
			LOGGER.info("No. of rows updated: {}", rowCount);
			delegate.getTransaction().commit();
			return rowCount;
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return 0;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> List<T> findByCriteria(Class<T> criteriaClass, Map<String, Object> predicateMap, String persistenseUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, false)) {
			EntityManager em = delegate.getDelegatee();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery(criteriaClass);
			List<Predicate> predicates = this.getPredicates(predicateMap, cb, cq.from(criteriaClass));
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			return em.createQuery(cq).getResultList();
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> List<T> selectByQuery( String persistenseUnit, String query) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, false)) {
			LOGGER.info("Inside Select By Query");			
			EntityManager em = delegate.getDelegatee();
			TypedQuery<T> q = (TypedQuery<T>) em.createQuery(query);
			List<T> results = q.getResultList();
			return results;
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;

		}
	}
	
	@Override
	public <T extends BaseEntity> List<T> selectByQuery( String persistenseUnit, String query ,int startIndex, int limit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, false)) {
			LOGGER.info("Inside Select By Query");			
			EntityManager em = delegate.getDelegatee();
			TypedQuery<T> q = (TypedQuery<T>) em.createQuery(query);
			q.setFirstResult(startIndex);
			q.setMaxResults(limit);
			List<T> results = q.getResultList();
			return results;
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> List<T> findByNamedQuery(Class<T> criteriaClass, String namedQuery,
			String persistenseUnit, Object... queryParams) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, false)) {
			TypedQuery<T> tquery = delegate.getDelegatee().createNamedQuery(namedQuery, criteriaClass);
			this.setQueryParameters(tquery, queryParams);
			return tquery.getResultList();
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}

	/*
	 * 
	    * @param inParams
	    *            as IN criteria.
	    * @param criteriaClass
	    *            EntityName as a Class
	    * @return List of entity
	    * 
	    */
	   @Override
	   public <T extends BaseEntity> List<T> findByCriteriaWithINParams(Map<String, List<Object>> inParams,
	           Class<T> criteriaClass, String persistenseUnit) {
	    try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, false)) {
	   EntityManager entityManager = delegate.getDelegatee();
	   String entityAttr = inParams.keySet().iterator().next();
	               List<Object> inParamlist = inParams.entrySet().iterator()
	                       .next().getValue();
	               CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	               CriteriaQuery<T> cq = cb.createQuery(criteriaClass);
	               Root<T> persistentObj = cq.from(criteriaClass);
	               cq.select(persistentObj).where(
	                       persistentObj.get(entityAttr).in(inParamlist));
	               TypedQuery<T> tquery = entityManager.createQuery(cq);
	               return tquery.getResultList();
	 
	    } catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
	   LOGGER.error("Exception while updating :", ex);
	   return null;
	  }
	   }

	/**
	 * {@inheritDoc}
     */
	@Override
	public <T extends BaseEntity> List<T> findAll(Class<T> entityClass, String persistenceUnit,
												  int startIndex, int limit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenceUnit, false)) {
			EntityManager em = delegate.getDelegatee();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery(entityClass);
			cq.select(cq.from(entityClass));
			TypedQuery<T> typedQuery = em.createQuery(cq);
			typedQuery.setMaxResults(limit);
			typedQuery.setFirstResult(startIndex);
			return typedQuery.getResultList();
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}

	@Override
	public <T extends BaseEntity> Long count(Class<T> entityClass, String persistenceUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenceUnit, false)) {
			EntityManager em = delegate.getDelegatee();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			cq.select(cb.count(cq.from(entityClass)));
			return em.createQuery(cq).getSingleResult();
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}

	@Override
	public <T extends BaseEntity> Long countByCriteria(Class<T> entityClass, Map<String, Object> queryParams,
													   String persistenceUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenceUnit, false)) {
			EntityManager em = delegate.getDelegatee();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery(entityClass);
			List<Predicate> predicates = this.getPredicates(queryParams, cb, cq.from(entityClass));
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			return Long.valueOf(em.createQuery(cq).getResultList().size());
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}

	@Override
	public <T extends BaseEntity> List<T> findByCriteria(Class<T> criteriaClass, Map<String, Object> queryParams,
														 int startIndex, int limit, String persistenceUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenceUnit, false)) {
			EntityManager em = delegate.getDelegatee();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery(criteriaClass);
			List<Predicate> predicates = this.getPredicates(queryParams, cb, cq.from(criteriaClass));
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			TypedQuery<T> typedQuery = em.createQuery(cq);
			typedQuery.setFirstResult(startIndex);
			typedQuery.setMaxResults(limit);
			return typedQuery.getResultList();
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E> E getScalarResultByNamedQuery(Class<E> scalarResultClass, String namedQuery,
											 String persistenseUnit, Object... queryParams) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, false)) {
			TypedQuery<E> tquery = delegate.getDelegatee().createNamedQuery(namedQuery, scalarResultClass);
			this.setQueryParameters(tquery, queryParams);
			return tquery.getSingleResult();
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> List<T> findAll(Class<T> entityClass, String persistenseUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, false)) {
			EntityManager em = delegate.getDelegatee();
			CriteriaQuery<T> cq = em.getCriteriaBuilder().createQuery(entityClass);
			return em.createQuery(cq.select(cq.from(entityClass))).getResultList();
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> void delete(T entityInstance, String persistenseUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, true)) {
			EntityManager em = delegate.getDelegatee();
			em.remove(em.contains(entityInstance) ? entityInstance : em.merge(entityInstance));
			delegate.getTransaction().commit();
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> int deleteByCriteria(Class<T> entity, Map<String, Object> predicateMap,
													   String persistenseUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, true)) {
			EntityManager em = delegate.getDelegatee();
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaDelete<T> delete = cb.createCriteriaDelete(entity);
			List<Predicate> predicates = this.getPredicates(predicateMap, cb, delete.from(entity));
			delete.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			int rowCount = em.createQuery(delete).executeUpdate();
			LOGGER.info("deleteByCriteria: No. of rows deleted: {}", rowCount);
			delegate.getTransaction().commit();
			return rowCount;
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return 0;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> int deleteAll(Class<T> entity, String persistenseUnit) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, true)) {
			EntityManager em = delegate.getDelegatee();
			int rowCount = em.createQuery(em.getCriteriaBuilder().createCriteriaDelete(entity)).executeUpdate();
			LOGGER.info("deleteAll: No. of rows deleted: {}", rowCount);
			delegate.getTransaction().commit();
			return rowCount;
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return 0;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T extends BaseEntity> int deleteByNamedQuery(Class<T> entityClass, String namedQuery,
														 String persistenseUnit, Object... queryParams) {
		try (EntityManagerDelegate delegate = (EntityManagerDelegate) this.providerService
				.getEntityManagerDelegateByUnit(persistenseUnit, true)) {
			TypedQuery<T> tquery = delegate.getDelegatee().createNamedQuery(namedQuery, entityClass);
			this.setQueryParameters(tquery, queryParams);
			int rowsDeleted = tquery.executeUpdate();
			LOGGER.info("deleteByNamedQuery: No. of rows deleted: {}", rowsDeleted);
			delegate.getTransaction().commit();
			return rowsDeleted;
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			LOGGER.error("Exception while updating :", ex);
			return 0;
		}
	}

	/**
	 * Life cycle method of PersistenceServiceImpl. This method automatically
	 * gets invoked whenever bundle is deployed.
	 * 
	 * @param context
	 *            the ComponentContext
	 */
	@Activate
	protected void activate(final ComponentContext context) {
		LOGGER.info("EntityManagerProviderFactoryImpl: {}", this.emProvider);
		this.isNamedQuerySupported = PropertiesUtil.toBoolean(context.getProperties().get(NAMED_QUERY_SUPPORTED),
				false);
		LOGGER.info("NamedQuery Supported: {}", this.isNamedQuerySupported);
	}

	/**
	 * Life cycle method of PersistenceServiceImpl. This method automatically
	 * gets invoked whenever bundle is un-deployed or the configurations are
	 * changed.
	 * 
	 * @param context
	 *            the context
	 */
	@Deactivate
	protected void deactivate(final ComponentContext context) {
		LOGGER.info("Deactivating PersistenceService!!");
	}

	/**
	 * This method sets the positional query parameters passed by the caller.
	 * 
	 * @param tquery
	 * @param queryParams
	 */
	private void setQueryParameters(TypedQuery<?> tquery, Object... queryParams) {
		if (queryParams != null && queryParams.length > 0) {
			for (int i = 0; i < queryParams.length; i++) {
				Object queryParam = queryParams[i];
				// Positional parameters always starts with 1.
				tquery.setParameter(i + 1, queryParam);
			}
		}
	}

	/**
	 * Prepares the Predicate List.
	 * 
	 * @param predicateMap
	 * @param cb
	 * @param root
	 * @return List<Predicate>
	 */
	private <T extends BaseEntity> List<Predicate> getPredicates(Map<String, Object> predicateMap, CriteriaBuilder cb,
			Root<T> root) {
		List<Predicate> predicates = new ArrayList<>();
		for (Entry<String, ?> entry : predicateMap.entrySet()) {
			predicates.add(cb.equal(root.get(entry.getKey()), entry.getValue()));
		}
		return predicates;
	}
}