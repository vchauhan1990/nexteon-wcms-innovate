package com.nexteon.innovate.persistence.api;

import java.io.Serializable;

/**
 * Marker Interface and Super of all the JPA Entities. This interface denotes
 * that all implementations of it are Serializable.
 * 
 * @author Rakesh.Kumar
 */
public interface BaseEntity extends Serializable {
}
