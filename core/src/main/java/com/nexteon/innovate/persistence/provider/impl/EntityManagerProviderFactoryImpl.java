package com.nexteon.innovate.persistence.provider.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.ConfigurationPolicy;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyOption;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.classloader.DynamicClassLoaderManager;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.jpa.PersistenceProvider;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.persistence.provider.EntityManagerProviderFactory;
import com.nexteonsolutions.aembootstrap.core.persistence.api.LoggingLevel;

/**
 * This class provides the EntityManager references to the OSGI components/services.
 *
 * @author Rakesh.Kumar, NextEon Technologies.
 * @since 15.11.2014
 */
@Service(EntityManagerProviderFactory.class)
@Component(metatype = true, immediate = true, label = "InnovateToInspire EntityManagerProviderFactory Service", configurationFactory = true, policy = ConfigurationPolicy.REQUIRE)
@Properties({ @Property(name = Constants.SERVICE_VENDOR, value = "NextEon IT Solutions") })
public class EntityManagerProviderFactoryImpl implements EntityManagerProviderFactory {

	/**
	 * Constant for declaring persistence provider.
	 */
	public static final String PERSISTENCE_PROVIDER = "javax.persistence.provider";
	
	/**
	 * Logger for this class.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(EntityManagerProviderFactoryImpl.class);
	
	/**
	 * Configurable Properties Starts.
	 */
	@Property(label = "Persistence Unit", description = "Database persistence unit to use (JPA).", value = "EESLINNOVATE")
	public static final String PERSISTENCE_UNIT = "persistence.unit";

	/** The Constant JDBC_DRIVER. */
	@Property(label = "JDBC Driver", description = "JDBC driver FQCN", value = "com.mysql.jdbc.Driver")
	public static final String JDBC_DRIVER = "jdbc.driver";
	
	
	/** The Constant JDBC_URL. */
	@Property(label = "JDBC URL", description = "Format: jdbc:<db>://<host>", value = "jdbc:mysql://<host>:<port>/<schema>")
	public static final String JDBC_URL = "jdbc.url";

	/** The Constant JDBC_USER. */
	@Property(label = "JDBC User", description = "Username to login to the database", value = "root")
	public static final String JDBC_USER = "jdbc.user";

	/** The Constant JDBC_PASSWORD. */
	@Property(label = "JDBC Password", description = "Password to login to the database")
	public static final String JDBC_PASSWORD = "jdbc.password";

    /** The Constant LOGGING_LEVEL. */
    @Property(label = "Logging Level", name = "logging.level", value = LoggingLevel.FINEST, description = "JPA Logging level", 
    	options = {
            @PropertyOption(name = LoggingLevel.OFF, value = LoggingLevel.OFF),
            @PropertyOption(name = LoggingLevel.SEVERE, value = LoggingLevel.SEVERE),
            @PropertyOption(name = LoggingLevel.WARNING, value = LoggingLevel.WARNING),
            @PropertyOption(name = LoggingLevel.INFO, value = LoggingLevel.INFO),
            @PropertyOption(name = LoggingLevel.CONFIG, value = LoggingLevel.CONFIG),
            @PropertyOption(name = LoggingLevel.FINE, value = LoggingLevel.FINE),
            @PropertyOption(name = LoggingLevel.FINER, value = LoggingLevel.FINER),
            @PropertyOption(name = LoggingLevel.FINEST, value = LoggingLevel.FINEST),
            @PropertyOption(name = LoggingLevel.ALL, value = LoggingLevel.ALL)
        })
    public static final String LOGGING_LEVEL = "logging.level";

	/** The Constant PERSISTENCE_XML. */
	@Property(label = "JDBC User", description = "Username to login to the database", value = "persistence.xml")
	public static final String PERSISTENCE_XML = "persistance.xml";
    
    /**
     * Holder of JPA properties.
     */
    private JpaProperties jpaProperties;
    
	/**
	 * EntityManagerFactory instance created from configurations.
	 */
	private EntityManagerFactory entityManagerFactory;

	private String persistenceUnit;
	
	@Reference
	private DynamicClassLoaderManager dclm;
	
	/**
	 * Life cycle method of EntityManagerProviderFactoryImpl. This method automatically
	 * gets invoked whenever bundle is deployed. Here it creates the
	 * EntityManagerFactory.
	 * 
	 * @param configMap
	 *            the ComponentContext
	 */
	@Activate
	protected void activate(Map<String, String> configMap) {
		this.persistenceUnit = configMap.get(PERSISTENCE_UNIT);
		String jdbcDriver = configMap.get(JDBC_DRIVER);
		LOGGER.info("jdbcDriver: {}", jdbcDriver);
		String jdbcUrl = configMap.get(JDBC_URL);
		String jdbcUser = configMap.get(JDBC_USER);
		String jdbcPassword = configMap.get(JDBC_PASSWORD);
		String loggingLevel = configMap.get(LOGGING_LEVEL);
		String persistenseXml = configMap.get(PERSISTENCE_XML);
		this.jpaProperties = new JpaProperties(persistenceUnit, jdbcDriver, jdbcUrl, jdbcUser, jdbcPassword,
				loggingLevel, persistenseXml);
		LOGGER.info("Configurations: {}", this.jpaProperties);
		// Create Entity Manager Factory
		this.createEntityManagerFactory();
		if (this.entityManagerFactory == null) {
			LOGGER.info("EntityManagerFactory could not be created!!");
		} else {
			LOGGER.info("EntityManagerFactory created successfully!!");
		}
	}

	/**
	 * Creates the EntityManagerFactory using configurations.
	 */
	private void createEntityManagerFactory() {
		final Map<String, Object> jpaPropertyMap = new HashMap<>();
		jpaPropertyMap.put(PersistenceUnitProperties.JDBC_DRIVER, this.jpaProperties.getJdbcDriver());
		jpaPropertyMap.put(PersistenceUnitProperties.JDBC_URL, this.jpaProperties.getJdbcUrl());
		jpaPropertyMap.put(PersistenceUnitProperties.JDBC_USER, this.jpaProperties.getJdbcUser());
		jpaPropertyMap.put(PersistenceUnitProperties.JDBC_PASSWORD, this.jpaProperties.getJdbcPassword());
		jpaPropertyMap.put(PersistenceUnitProperties.DDL_GENERATION, PersistenceUnitProperties.CREATE_OR_EXTEND);

		jpaPropertyMap.put(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_XML,
				"META-INF/" + this.jpaProperties.getPersistenseXml() );
		jpaPropertyMap.put(PersistenceUnitProperties.DDL_GENERATION_MODE,
				PersistenceUnitProperties.DDL_BOTH_GENERATION);
		jpaPropertyMap.put(PersistenceUnitProperties.LOGGING_LEVEL, this.jpaProperties.getLoggingLevel());
		jpaPropertyMap.put(PersistenceUnitProperties.CLASSLOADER, this.dclm.getDynamicClassLoader());
		// jpaPropertyMap.put(PersistenceUnitProperties.EXCEPTION_HANDLER_CLASS, JpaExceptionHandler.class.getName());
		jpaPropertyMap.put(PersistenceUnitProperties.DEPLOY_ON_STARTUP, "true");
		// Create Factory - starts.
		PersistenceProvider provider = new PersistenceProvider();
		jpaPropertyMap.put(PERSISTENCE_PROVIDER, provider.getClass());
		//this.entityManagerFactory = provider.createEntityManagerFactory(this.jpaProperties.getPersistenceUnit(),
		//		jpaPropertyMap);
		this.entityManagerFactory = provider.createEntityManagerFactory(this.jpaProperties.getPersistenceUnit(),
						jpaPropertyMap);	
		// Create Factory - ends.
	}

	/**
	 * Life cycle method of EntityManagerProviderFactoryImpl. This method automatically
	 * gets invoked whenever bundle is un-deployed or the configurations are
	 * changed. Here it closes the EntityManagerFactory.
	 * 
	 * @param context
	 *            the context
	 */
	@Deactivate
	protected void deactivate(final ComponentContext context) {
		// Close the EntityManagerFactory.
		if (this.entityManagerFactory != null) {
			this.entityManagerFactory.close();
			LOGGER.info("EntityManagerFactory closed successfully!!");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getEntityManager() {
		return this.entityManagerFactory.createEntityManager();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManagerDelegate getEntityManagerDelegate(boolean withTxn) {
		return new EntityManagerDelegate(this.getEntityManager(), withTxn);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityTransaction beginTransaction(EntityManager em) {
		EntityTransaction txn = em.getTransaction();
		txn.begin();
		return txn;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeEntityManagerAndTxn(EntityManager em, EntityTransaction txn) {
		if (txn != null && txn.isActive()) {
			txn.rollback();
		}
		if (em != null) {
			em.close();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void closeEntityManager(EntityManager em) {
		if (em != null) {
			em.close();
		}
	}

	@Override
	public String getPersistenseUnit() {
		return this.persistenceUnit;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "EntityManagerProviderFactoryImpl for PU: " + this.jpaProperties.getPersistenceUnit();
	}

	/**
	 * Invocation handler that catches all the exceptions thrown by the
	 * underlying JPA provider and throws a common PersistenceException.
	 * 
	 */
	public static class PersistenceExceptionHandler implements InvocationHandler {

		private final EntityManager target;

		public PersistenceExceptionHandler(EntityManager target) {
			this.target = target;
		}

		private static final List<String> EM_METHODS = new ArrayList<>();

		static {
			EM_METHODS.add("persist");
			EM_METHODS.add("merge");
			EM_METHODS.add("createQuery");
			EM_METHODS.add("remove");
			EM_METHODS.add("createNamedQuery");
		}

		/**
		 * A simple factory method to hide the messiness of creating the proxy
		 * from clients.
		 *
		 * @param em
		 *            the target EntityManager
		 * @return a proxied EntityManager
		 */
		public static EntityManager createProxy(EntityManager em) {
			return (EntityManager) Proxy.newProxyInstance(em.getClass().getClassLoader(),
					new Class[] { EntityManager.class, JpaEntityManager.class }, new PersistenceExceptionHandler(em));
		}

		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			// Invoke method on target EntityManager.
			EntityTransaction txn = null;
			Object returnValue = null;
			try {
				String methodName = method.getName();
				if (EM_METHODS.contains(methodName)) {
					txn = this.target.getTransaction();
					txn.begin();
					returnValue = method.invoke(this.target, args);
					txn.commit();
				} else {
					returnValue = method.invoke(this.target, args);
				}
				return returnValue;
			} catch (Exception ex) {
				LOGGER.error("ERROR: ===="+ex);
				return null;
			} finally {
				if (txn != null && txn.isActive()) {
					txn.rollback();
				}
				this.target.close();
			}
		}
	}

}