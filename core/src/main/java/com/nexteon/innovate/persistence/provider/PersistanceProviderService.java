package com.nexteon.innovate.persistence.provider;


/**
 *
 */
public interface PersistanceProviderService {

    Object getEntityManagerDelegateByUnit(String unit, boolean withTxn);

}
