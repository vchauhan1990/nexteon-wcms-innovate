package com.nexteon.innovate.persistence.provider.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.persistence.provider.EntityManagerProviderFactory;
import com.nexteon.innovate.persistence.provider.PersistanceProviderService;

@Service(PersistanceProviderService.class)
@Component(label = "InnovateToInspire Entity Factory Consumer", description = "Factory Consumer for InnovateToInspire", immediate = true,
        metatype = true, enabled = true)
public class PersistanceProviderServiceImpl implements PersistanceProviderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersistanceProviderServiceImpl.class);

    @Reference(policy = ReferencePolicy.DYNAMIC, referenceInterface = EntityManagerProviderFactory.class,
    cardinality = ReferenceCardinality.MANDATORY_MULTIPLE)
    private List<EntityManagerProviderFactory> entityManagerProviderFactoryList;


    /**
     * Adding EntityManagerProviderFactory service instance
     * @param factory
     */
    protected synchronized void bindEntityManagerProviderFactory(final EntityManagerProviderFactory factory) {
        if (this.entityManagerProviderFactoryList == null) {
            this.entityManagerProviderFactoryList = new ArrayList<>();
        }
        this.entityManagerProviderFactoryList.add(factory);
    }

    /**
     * Removing EntityManagerProviderFactory service instance
     * @param factory
     */
    protected synchronized void unbindEntityManagerProviderFactory(final EntityManagerProviderFactory factory) {
        this.entityManagerProviderFactoryList.remove(factory);
    }

    /**
     * Get entity provider instance by persistence unit name.
     *
     * @param unit
     * @param withTxn
     * @return
     */
    @Override
    public Object getEntityManagerDelegateByUnit(String unit, boolean withTxn) {
        if (this.entityManagerProviderFactoryList != null) {
            EntityManagerProviderFactory providerFactory = this.entityManagerProviderFactoryList
                    .stream().filter(entity -> entity.getPersistenseUnit().equals(unit)).findFirst().get();
            return providerFactory.getEntityManagerDelegate(withTxn);
        }
        return null;
    }
}
