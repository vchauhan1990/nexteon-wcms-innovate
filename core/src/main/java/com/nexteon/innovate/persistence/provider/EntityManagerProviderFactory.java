package com.nexteon.innovate.persistence.provider;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.nexteon.innovate.persistence.provider.impl.EntityManagerDelegate;

/**
 * This interface declares method that will provide the EntityManager references
 * to the OSGI components/services.
 *
 * @author Rakesh.Kumar, NextEon Technologies.
 * @since 15.11.2014
 */
public interface EntityManagerProviderFactory {

	/**
	 * Returns a new application-managed EntityManager. This method returns a
	 * new EntityManager instance each time it is invoked. The isOpen method
	 * will return true on the returned instance.
	 * 
	 * @return EntityManager
	 * @throws IllegalStateException
	 *             - if the entity manager factory has been closed
	 */
	EntityManager getEntityManager();

	/**
	 * Returns a Delegate to a new application-managed EntityManager. This
	 * method returns a new EntityManagerDelegate instance each time it is
	 * invoked. The isOpen method will return true on the wrapped EntityManager
	 * instance.
	 * 
	 * @param withTxn
	 *            if the transaction is required
	 * @return EntityManagerDelegate
	 * @throws IllegalStateException
	 *             - if the entity manager factory has been closed
	 */
	EntityManagerDelegate getEntityManagerDelegate(boolean withTxn);

	/**
	 * Method to get the EntityTransaction and call the begin method.
	 * 
	 * @param em
	 *            the entity manager
	 * @return EntityTransaction
	 */
	EntityTransaction beginTransaction(EntityManager em);

	/**
	 * Method takes care of closing EntityManage and Active EntityTransaction.
	 * 
	 * @param em
	 *            the entity manager
	 * @param txn
	 *            the entity transaction
	 */
	void closeEntityManagerAndTxn(EntityManager em, EntityTransaction txn);

	/**
	 * Method takes care of closing EntityManage.
	 * 
	 * @param em
	 *            the entity manager
	 */
	void closeEntityManager(EntityManager em);

	/**
	 * Get persistence unit name
	 *
	 * @return
     */
	String getPersistenseUnit();
}
