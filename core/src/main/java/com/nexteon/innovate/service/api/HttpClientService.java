package com.nexteon.innovate.service.api;

public interface HttpClientService {
	public String readResponse(String urlString);
}
