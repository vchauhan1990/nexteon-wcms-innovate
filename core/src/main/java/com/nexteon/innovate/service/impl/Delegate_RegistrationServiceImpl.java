package com.nexteon.innovate.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.persistence.api.PersistenceService;
import com.nexteon.innovate.persistence.entity.Delegate_Registration;
import com.nexteon.innovate.service.api.Delegate_RegistrationService;

@Service(Delegate_RegistrationService.class)
@Component(immediate = true, metatype = false, label = "NextEon Innovate Registration Service", description = "Innovate Registration service to perform CRUD Ops")
@Property(name = Constants.SERVICE_DESCRIPTION, value = "NextEon Innovate Registration for Delegates")
public class Delegate_RegistrationServiceImpl implements Delegate_RegistrationService{

	private static final Logger LOGGER = LoggerFactory.getLogger(Delegate_RegistrationServiceImpl.class);

	private static final String UNIT = "EESLINNOVATE";

	@Reference
	private PersistenceService persistenceService;

	@Override
	public boolean insertRegistration(Delegate_Registration registrationPerforma) {
		try {
			this.persistenceService.insert(registrationPerforma, UNIT);
			return true;
		} catch (Exception ex) {
			LOGGER.error("Medical Form Service Exception -> ", ex);
		}
		return false;
	}

	@Override
	public List<Delegate_Registration> getAllRegistrations() {
		List<Delegate_Registration> registrationList = null;
		try {
			registrationList = this.persistenceService.findAll(Delegate_Registration.class, UNIT);
		} catch (Exception ex) {
			LOGGER.error("RegistrationPerforma Form Service Exception -> ", ex);
		}
		return registrationList;
	}

	@Override
	public Delegate_Registration getRegistrationByName(String delegateName) {
		Delegate_Registration registration = null;
		try {
			Map<String,Object> criteria = new HashMap<>();
			criteria.put("delegateName", delegateName);
			registration = this.persistenceService.findByCriteria(Delegate_Registration.class, criteria, UNIT).get(0);
		} catch (Exception ex) {
			LOGGER.error("RegistrationPerforma Service Exception -> ", ex);
		}
		return registration;
	}

	@Override
	public Delegate_Registration getRegistrationByEmailId(String delegateEmail) {
		Delegate_Registration registration = null;
		try {
			Map<String,Object> criteria = new HashMap<>();
			criteria.put("delegateEmail", delegateEmail);
			registration = this.persistenceService.findByCriteria(Delegate_Registration.class, criteria, UNIT).get(0);
		} catch (Exception ex) {
			LOGGER.error("RegistrationPerforma Service Exception -> ", ex);
		}
		return registration;
	}

	@Override
	public Delegate_Registration getRegistrationById(int delegateId) {
		Delegate_Registration registration = null;
		try {
			Map<String,Object> criteria = new HashMap<>();
			criteria.put("delegateId", delegateId);
			registration = this.persistenceService.findByCriteria(Delegate_Registration.class, criteria, UNIT).get(0);
		} catch (Exception ex) {
			LOGGER.error("RegistrationPerforma Service Exception -> ", ex);
		}
		return registration;
	}
	
	
}
