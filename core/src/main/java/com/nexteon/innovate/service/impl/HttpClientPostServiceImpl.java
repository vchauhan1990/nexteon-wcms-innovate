package com.nexteon.innovate.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.service.api.HttpClientPostService;

@Service(HttpClientPostService.class)
@Component(immediate = true, metatype = false, label = "Http Post Service For Medical Portal", description = "Medical Portal HttpPOSTService to hit Rajasthan SSO")
@Property(name = Constants.SERVICE_DESCRIPTION, value = "Http Post Service to hit Rajasthan SSO Medical Portal")

public class HttpClientPostServiceImpl implements HttpClientPostService{

private static Logger LOGGER = LoggerFactory.getLogger(HttpClientPostServiceImpl.class);

	
	@Override
	public JSONObject getResponse(JSONObject requestObject, String URLIntake) {
		LOGGER.info("Inside readResponse");
		// String response = null;
		String output = null;
		HttpURLConnection conn = null;
		String response = null;
		JSONObject responseObject = null;
		try {

			URL url = new URL(URLIntake);
			LOGGER.info("Opening connection for URL");
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			// String input1 = "{\"_parameters\": [{ \"getiview\": { \"name\":
			// \"projrep\", \"axpapp\": \"wmstest\",
			// \"username\":\"mobileuser\",\"password\":\"827ccb0eea8a706c4c34a16891f84e7b\",\"seed\":\"\",\"s\":
			// \"\", \"pageno\": \"1\",\"pagesize\": \"5\",\"sqlpagination\":
			// \"true\", \"params\":
			// {\"pdistrict\":\"ALL\",\"ppansamiti\":\"ALL\",\"pvillage\":\"ALL\",\"pactivity\":\"ALL\"}}}]}";
			OutputStream os = conn.getOutputStream();
			os.write(requestObject.toString().getBytes());
			os.flush();

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			while ((output = br.readLine()) != null) {
				response = output;
			}
			LOGGER.info("Result: "+ response);
			responseObject = new JSONObject(response);
			
		} catch (MalformedURLException e) {

			LOGGER.error("Error in post: "+ e);

		} catch (Exception e) {

			LOGGER.error("Error in post: "+ e);

		} finally {
			conn.disconnect();
		}
		return responseObject;
	}


	@Override
	public void postToUrl(String userDetails, String URLIntake) {
		LOGGER.info("Inside post");
		// String response = null;
		String output = null;
		HttpURLConnection conn = null;
		String response = null;
		JSONObject responseObject = null;
		try {
			
			URL url = new URL(URLIntake);
			LOGGER.info("Opening connection for URL");
			
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(true);
			conn.setRequestMethod("POST");
			
			conn.setRequestProperty("Content-Type", "application/json");
			
			String urlParameters = "userdetails="+ userDetails;
			
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			while ((output = br.readLine()) != null) {
				response = output;
			}
			LOGGER.info("Result: "+ response);
			responseObject = new JSONObject(response);
			
		} catch (MalformedURLException e) {

			LOGGER.error("Error in post: "+ e);

		} catch (Exception e) {

			LOGGER.error("Error in post: "+ e);

		} finally {
			conn.disconnect();
		}
	}

	
}
