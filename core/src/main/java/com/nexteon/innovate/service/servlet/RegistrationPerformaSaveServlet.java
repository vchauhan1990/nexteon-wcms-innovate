package com.nexteon.innovate.service.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.persistence.entity.Delegate_Registration;
import com.nexteon.innovate.service.api.Delegate_RegistrationService;
import com.nexteonsolutions.aembootstrap.core.utils.RequestUtil;

@SlingServlet(label = "Registration Performa Save Servlet",
			description = "Registration Performa Save Servlet for saving information of delegates",
			paths = { "/bin/nexteon/registration/save" },
			methods = { HttpConstants.METHOD_POST },
			metatype = true)
public class RegistrationPerformaSaveServlet extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3422574448903676895L;
	public static final Logger LOGGER = LoggerFactory.getLogger(RegistrationPerformaSaveServlet.class);

	@Reference
	private Delegate_RegistrationService registrationPerformaService;


	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		Delegate_Registration registration = new Delegate_Registration();
		
		registration.setCreateDate(new Date());
		registration.setDelegateName(request.getParameter("delegateName"));
		registration.setDesignation(request.getParameter("delegateDesignation"));
		registration.setOrganisation(request.getParameter("delegateOrganisation"));
		registration.setAddress(request.getParameter("delegateAddress"));
		registration.setCity(request.getParameter("delegateCity"));
		registration.setPinCode(request.getParameter("delegatePin"));
		registration.setPhone_Number(request.getParameter("delegatePhone"));
		registration.setFax(request.getParameter("delegateFax"));
		registration.setMobileNumber(request.getParameter("delegateMobile"));
		registration.setEmail(request.getParameter("delegateEmail"));

		boolean status = registrationPerformaService.insertRegistration(registration);
		
		if (status) {
			try {
				request.setAttribute("successMsg", "Thank You! Your registration has been completed successfully.");
				RequestUtil.handleForward(request, response, "/content/raj/innovate-to-inspire/en/registration.html");
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ServletException e) {
				e.printStackTrace();
			}
		}else{
			try {
				request.setAttribute("errrorMsg", "Something went wrong while saving your details. This might happen if your mobile number or email is already registered.");
				RequestUtil.handleForward(request, response, "/content/raj/innovate-to-inspire/en/registration.html");
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ServletException e) {
				e.printStackTrace();
			}
		}
	}
}
