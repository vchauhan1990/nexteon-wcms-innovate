package com.nexteon.innovate.service.api;

import java.util.List;

import com.nexteon.innovate.persistence.entity.Delegate_Registration;

public interface Delegate_RegistrationService {

	public boolean insertRegistration(Delegate_Registration registrationPerforma);
	public List<Delegate_Registration> getAllRegistrations();
	public Delegate_Registration getRegistrationByName(String delegateName);
	public Delegate_Registration getRegistrationByEmailId(String delegateEmail);
	public Delegate_Registration getRegistrationById(int delegateId);

	
}