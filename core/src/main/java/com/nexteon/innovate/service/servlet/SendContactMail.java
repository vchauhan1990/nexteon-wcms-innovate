package com.nexteon.innovate.service.servlet;

import java.io.IOException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.nexteon.innovate.common.MailConfig;
import com.nexteonsolutions.aembootstrap.core.utils.RequestUtil;

@SlingServlet(label = "Send Mail for Contact Servlet", description = "Send Mail for Contact Servlet for mailing contact details", paths = {
		"/bin/nexteon/mail/send" }, methods = { HttpConstants.METHOD_POST }, metatype = true)
public class SendContactMail extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4085249273676763033L;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				sendMail(request,response);
			}
		}).start();
		try {
			request.setAttribute("successMsg", "Thank you for contacting us. We will get back to shortly.");
			RequestUtil.handleForward(request, response, "/content/raj/innovate-to-inspire/en/contact-us.html");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}
	
	private void sendMail(SlingHttpServletRequest request, SlingHttpServletResponse response){
		MimeMessage mimeMessage = new MailConfig().mimeMessage();
		try {
			mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(request.getParameter("emailId")));
		
		mimeMessage.setSubject("New Contact Request :: "+request.getParameter("subject"));
		StringBuilder sb=new StringBuilder();
		sb.append("Dear Team,");
		sb.append("<br/><br/>You have received a contact request from:");
		sb.append("<br/><br/><b>Name: </b>"+request.getParameter("fullName"));
		sb.append("<br/><br/><b>Subject: </b>"+request.getParameter("subject"));
		sb.append("<br/><br/><b>Email: </b>"+request.getParameter("emailId"));
		sb.append("<br/><br/><b>Message: </b>"+request.getParameter("message"));
		sb.append("<br/><br/>Thank You");
		mimeMessage.setContent(sb.toString(), "text/html; charset=utf-8");
		Transport.send(mimeMessage); 
		} catch (AddressException e) {
			request.setAttribute("errorMsg", e.getMessage());
			e.printStackTrace();
		} catch (MessagingException e) {
			request.setAttribute("errorMsg", e.getMessage());
			e.printStackTrace();
		}
		
	}
	

}
