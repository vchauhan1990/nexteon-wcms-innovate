package com.nexteon.innovate.service.api;

import org.apache.sling.commons.json.JSONObject;

public interface HttpClientPostService {

	public JSONObject getResponse(JSONObject requestObject,String URLIntake);
	public void postToUrl(String userDetails,String URLIntake);
	
}
