package com.nexteon.innovate.service.impl;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.service.api.HttpClientService;

@Service(HttpClientService.class)
@Component(immediate = true, metatype = false, label = "Http Client Service For Medical Portal", description = "Http Client Service to hit Rajasthan SSO")
@Property(name = Constants.SERVICE_DESCRIPTION, value = "Http Client Service to hit Rajasthan SSO Medical Portal")

public class HttpClientServiceImpl  implements HttpClientService{

private static Logger LOGGER = LoggerFactory.getLogger(HttpClientServiceImpl.class);
	
	@Override
	public  String readResponse(String urlString) {
		LOGGER.info("Inside readResponse");
		String response = null;
		String output = null;
		  try {
			 
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			
			
			while ((output = br.readLine()) != null) {
				
				response = output;
			}
			
			conn.disconnect();

		  } catch (MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  }
		  return response;
		  	
		}
	
}
