package com.nexteon.innovate.service.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nexteon.innovate.persistence.entity.Delegate_Registration;
import com.nexteon.innovate.service.api.Delegate_RegistrationService;

@SlingServlet(label = "Registration Performa Get Servlet", description = "Registration Performa Save Servlet for getting information of delegates", paths = {
		"/bin/nexteon/registration/get" }, methods = { HttpConstants.METHOD_POST }, metatype = true)
public class GetRegistrationDetailsServlet extends SlingAllMethodsServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4338441144994313212L;

	public static final Logger LOGGER = LoggerFactory.getLogger(RegistrationPerformaSaveServlet.class);

	@Reference
	private Delegate_RegistrationService registrationPerformaService;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
		response.setContentType("application/json");
		PrintWriter out= response.getWriter();
		List<Delegate_Registration> list = registrationPerformaService.getAllRegistrations();
		try {
			JSONArray array = new JSONArray();

			for (int i=0;i<list.size();i++) {
				Delegate_Registration delegate = list.get(i);
				JSONObject json = new JSONObject();
				json.put("name", delegate.getDelegateName());
				json.put("designation", delegate.getDesignation());
				json.put("organisation", delegate.getDelegateName());
				json.put("address", delegate.getAddress());
				json.put("city", delegate.getCity());
				json.put("pin", delegate.getPinCode());
				json.put("phone", delegate.getPhone_Number());
				json.put("fax", delegate.getFax());
				json.put("email", delegate.getEmail());
				json.put("mobile", delegate.getMobileNumber());
				array.put(i, json);
			}
			out.println(array.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
